#!/bin/bash
# Usage:
# ./experiments/scripts/cough_NN_end2end.sh GPU NET DATASET [options args to {train,test}_net.py]
# DATASET is manchester_trainval, manchester_test.
#
# Example:
# ./experiments/scripts/cough_NN_end2end.sh gpu 0 NN_2_512_512 manchester

set -x
set -e

export PYTHONUNBUFFERED="True"

DEV=$1
DEV_ID=$2
NET=$3
DATASET=$4

array=( $@ )
len=${#array[@]}
EXTRA_ARGS=${array[@]:4:$len}
EXTRA_ARGS_SLUG=${EXTRA_ARGS// /_}

case $DATASET in
  manchester)
    TRAIN_DB="manchester_trainval"
    TEST_DB="manchester_test"
    PT_DIR="manchester"
    ITERS=10000
    ;;
  *)
    echo "No dataset given"
    exit
    ;;
esac

LOG="experiments/logs/cough_NN_end2end_${NET}_${EXTRA_ARGS_SLUG}.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

NN_test="${NET}_test"
NN_train="${NET}_train"

time python ./tools/train.py --device ${DEV} --device_id ${DEV_ID} \
  --dataset ${TRAIN_DB} \
  --iters ${ITERS} \
  --network ${NN_train} \
  ${EXTRA_ARGS}

set +x
NET_FINAL=`grep -B 1 "Training Ends" ${LOG} | grep "Wrote snapshot" | awk '{print $4}'`
set -x

echo Final net is "$NET_FINAL"

time python ./tools/test.py --device ${DEV} --device_id ${DEV_ID} \
  --weights ${NET_FINAL} \
  --dataset ${TEST_DB} \
  --network ${NN_test} \
  ${EXTRA_ARGS}
