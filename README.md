# Python Framework for Neural Network Training for Cough Detection
This is a tensorflow implementation of neural network training and testing for cough detection from audio.

####Author : Abinash Mohanty | Arizona State Univeristy
***

### Requirements: software

* Requirements for Tensorflow (see: [Tensorflow](https://www.tensorflow.org/))
* Python packages you might not have: `cython`, `python-opencv`, `easydict`

### Requirements: hardware

* For training, 3G of GPU memory is sufficient (using CUDNN)
* For running the demo, GPU is not necessary. 

### Installation

* Clone the coughbi repository (make sure to clone with --recursive)
```
#!shell
git clone --recursive https://amohant4@bitbucket.org/amohant4/coughbi.git
```
After cloning, the folder structure should look like:
 
	Data			# Training/Testing Data directory
	experiments		# Scripts/logs for experiments
	lib				# libraries needed
	output			# directory for trained models	
	tools			# python scripts assisting training/testing
	README.md

### Demo

* After successfully completing installation, you should be able to run a demo.
To run the demo

```
#!shell
cd $COUGHBI_ROOT
python ./tools/demo.py

```
Here `$COUGHBI_ROOT` is the directory where the project is cloned. The demo uses a pre-trained neural network to classify 40 (200ms long) clips containing cough and background. 
If everything is installed properly, you should see something like this :

![Screen Shot 2017-05-22 at 4.03.39 AM.png](https://bitbucket.org/repo/baaG7Kd/images/3057836865-Screen%20Shot%202017-05-22%20at%204.03.39%20AM.png)

### Implementation details: 

* The frame work uses **lib/python\_speech\_features** to generate the MFCC features from audio input. 
* **cough\_NN** contains all the functionality needed for training and testing a network using tensorFlow. 
* manchester data related functionality is handled in **datasets**
* the neural network architecture is defined in **networks**
* **cough\_data\_layer** implements the data handling for feeding in training / testing blobs for MFCC data.