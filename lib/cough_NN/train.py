# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

"""Train a Neural Network for cough detection."""

from cough_NN.config import cfg
from cough_data_layer.layer import coughDataLayer	#TODO
import numpy as np
import os
import tensorflow as tf
import sys

class SolverWrapper(object):

	def __init__(self, sess, network, dataset, output_dir):
		""" 
		Initialize the wrapper.
		"""
		self.net = network
		self.dataset = dataset
		self.output_dir = output_dir

		#For snapshoting
		self.saver = tf.train.Saver(max_to_keep=100)

	# TODO: Implement the snapshot mechanism to save the trained models
	def snapshot(self, sess, iter):
		"""
		Take snapshot to the network
		"""
		net = self.net
		if not os.path.exists(self.output_dir):
			os.makedirs(self.output_dir)

		infix = ('_' + cfg.TRAIN.SNAPSHOT_INFIX
				if cfg.TRAIN.SNAPSHOT_INFIX != '' else '')
		filename = (cfg.TRAIN.SNAPSHOT_PREFIX + infix + net.name +
					'_iter_{:d}'.format(iter+1) + '.ckpt')
		filename = os.path.join(self.output_dir, filename)

		self.saver.save(sess, filename)
		print 'Wrote snapshot to: {:s}'.format(filename)
	

	def train_model(self, sess, max_iters):

		data_layer = coughDataLayer(self.dataset)
						
		# Define the loss here 
		# Classification loss
		cls_score = self.net.get_output('cls_score')
		label = self.net.get_output('gt_labels')
		cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(cls_score, label))
		
		loss = cross_entropy		# Add loss components as needed here 
		
		# Optimizer setup
		global_step = tf.Variable(0, trainable=False)
		lr = tf.train.exponential_decay(cfg.TRAIN.LEARNING_RATE, global_step, 
												cfg.TRAIN.STEPSIZE, 0.1, staircase=True)
		
		momentum = cfg.TRAIN.MOMENTUM
		train_op = tf.train.MomentumOptimizer(lr, momentum).minimize(loss, global_step=global_step)
		
		#initialize variables
		sess.run(tf.initialize_all_variables())

		last_snapshot_iter = -1
		for iter in range(max_iters):
			# Get one batch 
			blobs = data_layer.forward()

			# Make one SGD update
			feed_dict={self.net.MFCC_Feat: blobs['features'], self.net.keep_prob: 0.5, self.net.gt_labels: blobs['labels']}

			run_options = None
			run_metadata = None
			loss_cls_value, _ = sess.run([cross_entropy, train_op], 
										feed_dict=feed_dict,
										options=run_options,
										run_metadata=run_metadata)

			if (iter+1) % (cfg.TRAIN.DISPLAY) == 0:
				print 'iter: %d / %d, loss_cls: %.4f, lr: %f'%\
					(iter+1, max_iters, loss_cls_value, lr.eval())

			if (iter+1) % cfg.TRAIN.SNAPSHOT_ITERS == 0:
				last_snapshot_iter = iter
				self.snapshot(sess, iter)

		if last_snapshot_iter != iter:
			self.snapshot(sess, iter)			

def train_net(network, dataset, output_dir, max_iters=cfg.TRAIN.MAX_ITERS):
	""" 
	Train neural network. 
	"""
	with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess: 
		sw = SolverWrapper(sess, network, dataset, output_dir)
		print 'Training Starts ... '
		sw.train_model(sess, max_iters)
		print 'Training Ends'
