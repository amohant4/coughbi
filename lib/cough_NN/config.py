# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

"""Cough Detection NN configuration.

This file specifies the default configuration for Cough Detection NN. DO NOT change values in this file. Instead write a config file (in yaml) and use cfg_from_file(yaml_file) to load it and override the default options. 

"""

import os
import os.path as osp
import numpy as np
from easydict import EasyDict as edict

__C = edict()
# to get the config options use: from cough_NN import cfg
cfg = __C

# Training Options
__C.TRAIN = edict()

# TRAINING DATASET AUDIO FILES
__C.TRAIN.INPUT_FILES = 'r4 r10'

# TRAINING BATCH SIZE
__C.TRAIN.BATCH_SIZE = 100

# TRAINING NUMBER OF EPOCHS
__C.TRAIN.NUM_EPOCHS = 1000

#TRAINING ITERATIONS		Look into it TODO	
__C.TRAIN.MAX_ITERS = 10000

# TRAIN DATA GENERATION
# RANDOM CROP UNCERTAININTY REGION IN ms (-+)
__C.TRAIN.RND_WINDOW = 0.025

# Iterations between snapshots
__C.TRAIN.SNAPSHOT_ITERS = 5000

# solver.prototxt specifies the snapshot path prefix, this adds an optional
# infix to yield the path: <prefix>[_<infix>]_iters_XYZ.caffemodel
__C.TRAIN.SNAPSHOT_PREFIX = 'manchester_'
__C.TRAIN.SNAPSHOT_INFIX = ''

# TRAINING OPTIONS
__C.TRAIN.LEARNING_RATE = 0.001
__C.TRAIN.MOMENTUM = 0.9
__C.TRAIN.GAMMA = 0.1
__C.TRAIN.STEPSIZE = 50000
__C.TRAIN.DISPLAY = 10
__C.IS_MULTISCALE = False


# TESTING OPTIONS
__C.TEST = edict()

# TESTING DATASET AUDIO FILES
__C.TEST.INPUT_FILES = 'r9'

# MISC
__C.IOU_HIGH = 0.7
__C.IOU_LOW = 0.01


# WINDOW IN WHICH THE TRAINING AND TESTING SAMPLES ARE GENERATED
__C.WINDOW_SIZE = 0.7

# FRAME SIZE IN ms
__C.FRAME_SIZE = 0.2

# FRAME STRIDE USED IN TRAINING AND TESTING SAMPLE GENERATION
__C.FRAME_STRIDE = 0.05

# SUB-WINDOW SIZE (EACH FRAME IS DIVIDED INTO N SUB-WINDOWS in ms)
__C.SUB_WINDOW_SIZE = 0.05

# SUB_WINDOW_STRIDE USED IN FEATURE GENERATION
__C.SUB_WINDOW_STRIDE = 0.05

# For reproducibility
__C.RNG_SEED = 3

# Root Directory of Project
__C.ROOT_DIR = osp.abspath(osp.join(osp.dirname(__file__), '..', '..'))

# Data Directory
__C.DATA_DIR = osp.abspath(osp.join(__C.ROOT_DIR, 'Data'))

# Default GPU Device ID
__C.GPU_ID = 0

# TRAINING DATA GENERATION MODE
# MODE CAN BE 1: 1 random crop / GT, 2 : 4 random crop / GT, 3 : central crop / GT
#__C.DATA_GEN_MODE = 1
__C.DATA_GEN_MODE = 2

# Place outputs under an experiments directory
__C.EXP_DIR = 'default'

def get_output_dir(database, weights_filename):
    """Return the directory where experimental artifacts are placed.
    If the directory does not exist, it is created.

    A canonical path is built using the name from an database and a network
    (if not None).
    """
    outdir = osp.abspath(osp.join(__C.ROOT_DIR, 'output', __C.EXP_DIR, database._name))
    #outdir = osp.abspath(osp.join(__C.ROOT_DIR, 'output', __C.EXP_DIR, 'manchester_trainval'))
    if weights_filename is not None:
        outdir = osp.join(outdir, weights_filename)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    return outdir

def _merge_a_into_b(a, b):
    """Merge config dictionary a into config dictionary b, clobbering the
    options in b whenever they are also specified in a.
    """
    if type(a) is not edict:
        return

    for k, v in a.iteritems():
        # a must specify keys that are in b
        if not b.has_key(k):
            raise KeyError('{} is not a valid config key'.format(k))

        # the types must match, too
        old_type = type(b[k])
        if old_type is not type(v):
            if isinstance(b[k], np.ndarray):
                v = np.array(v, dtype=b[k].dtype)
            else:
                raise ValueError(('Type mismatch ({} vs. {}) '
                                'for config key: {}').format(type(b[k]),
                                                            type(v), k))

        # recursively merge dicts
        if type(v) is edict:
            try:
                _merge_a_into_b(a[k], b[k])
            except:
                print('Error under config key: {}'.format(k))
                raise
        else:
            b[k] = v

#def cfg_from_file(filename):
#    """Load a config file and merge it into the default options."""
#    import yaml
#    with open(filename, 'r') as f:
#        yaml_cfg = edict(yaml.load(f))
#
#    _merge_a_into_b(yaml_cfg, __C)



