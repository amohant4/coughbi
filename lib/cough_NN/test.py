# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------
"""
Test a neural network for cough detection. 
"""

from cough_NN.config import cfg, get_output_dir
from cough_data_layer.layer import coughDataLayer	#TODO
import numpy as np
import tensorflow as tf

def test_net(sess, net, dataset):
	""" 
	Test a given net for dataset provided.
	"""
	data_layer = coughDataLayer(dataset)
	blobs = data_layer.get_test_blob()

	feed_dict={net.MFCC_Feat: blobs['features'], net.keep_prob: 1.0, net.gt_labels: blobs['labels']}
	run_options = None
	run_metadata = None

	correct_prediction = tf.equal(tf.argmax(net.get_output('cls_prob'), 1), tf.argmax(net.get_output('gt_labels'), 1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))	

	print 'Testing network ... '
	net_accuracy = sess.run([accuracy], feed_dict=feed_dict, options=run_options, run_metadata=run_metadata)
	print 'Network accuracy is {}'.format(net_accuracy)
	
