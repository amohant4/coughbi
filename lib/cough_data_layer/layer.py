# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

import numpy as np
from cough_NN.config import cfg, get_output_dir

class coughDataLayer(object):

	def __init__(self, dataset):
		"""
		Initialize the layer. 
		"""
		self._coughdb = dataset.gt_coughdb()
		self._shuffle_inds('cough')
		self._shuffle_inds('bg')

	def _shuffle_inds(self, db_name):
		"""
		Randomly permute the training dataset.
		"""
		self._num_cough = len(self._coughdb['cough']['features'])
		self._num_bg = len(self._coughdb['bg']['features'])
		if db_name == 'cough':
			self._cur_cough = 0
			self._perm_cough = np.random.permutation(np.arange(self._num_cough))
		elif db_name == 'bg':	
			self._cur_bg = 0
			self._perm_bg = np.random.permutation(np.arange(self._num_bg))

	def _get_next_minibatch_inds(self):
		""" 
		Return the indices for the next minibatch. 
		Mostly handling coughs and backgrounds separately is not necessaary but its handled so. 
		"""
		if (self._cur_cough + int(cfg.TRAIN.BATCH_SIZE/2) >= self._num_cough): 
			self._shuffle_inds('cough')
		
		if (self._cur_bg + int(cfg.TRAIN.BATCH_SIZE/2) >= self._num_bg):
			self._shuffle_inds('bg')

		db_inds_cough = self._perm_cough[self._cur_cough:self._cur_cough + int(cfg.TRAIN.BATCH_SIZE/2)]
		db_inds_bg = self._perm_bg[self._cur_bg:self._cur_bg + int(cfg.TRAIN.BATCH_SIZE/2)]
		self._cur_cough += int(cfg.TRAIN.BATCH_SIZE/2)
		self._cur_bg += int(cfg.TRAIN.BATCH_SIZE/2)

		return db_inds_cough, db_inds_bg

	def _get_next_minibatch(self):
		""" 
		Return the blob to be used for the next minibatch. 
		"""
		inds_cough, inds_bg = self._get_next_minibatch_inds()

		feat_c = [self._coughdb['cough']['features'][i] for i in inds_cough] 
		loc_c = [self._coughdb['cough']['locations'][i] for i in inds_cough]
		label_c = [self._coughdb['cough']['labels'][i] for i in inds_cough]
		
		feat_bg = [self._coughdb['bg']['features'][i] for i in inds_bg] 
		loc_bg = [self._coughdb['bg']['locations'][i] for i in inds_bg]
		label_bg = [self._coughdb['bg']['labels'][i] for i in inds_bg]

		feat = np.array(feat_c + feat_bg)
		loc = np.array(loc_c + loc_bg)
		label = label_c + label_bg

		labels = np.zeros((cfg.TRAIN.BATCH_SIZE, 2), dtype=np.float32)
		# convert labels from class indices to matrix format
		# 0 is for bg
		# 1 is for cough
		for i in xrange(cfg.TRAIN.BATCH_SIZE):
			labels[i][label[i]] = 1.0
		blob = {'features': feat}
		blob['locations'] = loc
		blob['labels'] = labels

		return blob

	def forward(self):			# Used during training process
		""" Get blobs and copy them into this layer's top blob vector."""
		blobs = self._get_next_minibatch()
		return blobs


	def get_test_blob(self):	# Used during testing process
		""" Get blobs for testing. """
		feat_c		= [self._coughdb['cough']['features'][i] for i in self._perm_cough] 
		loc_c		= [self._coughdb['cough']['locations'][i] for i in self._perm_cough]
		label_c 	= [self._coughdb['cough']['labels'][i] for i in self._perm_cough]
			
		feat_bg		= [self._coughdb['bg']['features'][j] for j in self._perm_bg] 
		loc_bg		= [self._coughdb['bg']['locations'][j] for j in self._perm_bg]
		label_bg 	= [self._coughdb['bg']['labels'][j] for j in self._perm_bg]

		feat = np.array(feat_c + feat_bg)
		loc = np.array(loc_c + loc_bg)
		label = label_c + label_bg

		# Randomize between cough and bg 
		num = len(feat)	
		rand_perm_ids = np.random.permutation(np.arange(num))
 
 		feat_rnd	= [feat[i] for i in rand_perm_ids]
		loc_rnd		= [loc[i] for i in rand_perm_ids]	
		label_rnd	= [label[i] for i in rand_perm_ids]	

		label_matrix = np.zeros((len(label_rnd), 2), dtype=np.float32)
		for i in xrange(len(label_rnd)):
			label_matrix[i][label_rnd[i]] = 1

		blob = { 'features': feat_rnd }
		blob['locations'] = loc_rnd
		blob['labels'] = label_matrix	

		return blob
