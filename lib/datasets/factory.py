# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

"""Factory method for easily getting imdbs by name."""

__sets = {}

# Add future datasets here
from datasets.manchester import manchester
import numpy as np

# setup manchester_<split>
for split in ['train', 'val', 'trainval', 'test']:
	name='manchester_{}'.format(split)
	__sets[name] = (lambda split=split : manchester(split))


def get_dataset(name):
	""" 
	Get an cough dataset by name. 
	"""
	if not __sets.has_key(name):
		raise KeyError('Unknown dataset: {}'.format(name))
	return __sets[name]()

def list_datasets():
	"""
	List all registered datasets
	"""
	return __sets.keys()



