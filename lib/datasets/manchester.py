# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

import os
import numpy as np
from cough_NN.config import cfg
import csv
import random
import scipy.io.wavfile as wav
import math
import cPickle
from python_speech_features import mfcc
from python_speech_features import delta
from python_speech_features import logfbank


class manchester():
	def __init__(self, split):
		self._name = 'manchester'
		self._split = split
		self._num_classes = 2
		self._fs = 8000		# TODO	
		self._dataset_path = self._get_default_path()
		self._classes = ('__background__',
						 'cough')

		assert os.path.exists(self._dataset_path), \
			'manchester dataset path doesnot exist: {}'.format(self._dataset_path)
		
		assert os.path.exists(self.cache_path), \
			'Dataset cache folder doesnot exist: {}'.format(self.cache_path)

	@property
	def cache_path(self):
		cache_path = os.path.abspath(os.path.join(cfg.DATA_DIR, 'cache'))
		if not os.path.exists(cache_path):
			print 'There is no cache folder. Creating cache directory at ' + cache_path
			os.makedirs(cache_path)
		return cache_path


	def _get_default_path(self):
		""" 
		Return the default path where manchester data is expected to be present. 
		"""
		return os.path.join(cfg.DATA_DIR, 'manchester')

	def _gt_fineTuned_coughdb(self):
		"""
		Return the database of groundd-truth cough data in the 
		List containing 3 things. 
		1 - Start point of cough
		2 - End point of cough
		3 - Input filename
		"""
		fileNames = cfg.TRAIN.INPUT_FILES.split()
		if self._split == 'test':
			fileNames = cfg.TEST.INPUT_FILES.split()

		gt_cdb = [] 
		for names in fileNames:
			# READ .csv for the GT cough data
			# It is assumed thats the .csv files have fine tuned cough location after pre-processing
			path = os.path.join(self._dataset_path, names + '_fineTuned.csv')
			with open(path, 'rb') as csvfile:
				lines = csv.reader(csvfile, delimiter=',')
				gt_fineTuned_coughdb = list(lines)	
				gt_fineTuned_coughdb = [[int(x) for x in y] for y in gt_fineTuned_coughdb]	
				for m in gt_fineTuned_coughdb:
					m.append(names)		# Append a the file name along with cough for future reference
				print 'Number of GT coughs in file {} is {}'.format(names, len(gt_fineTuned_coughdb))
				gt_cdb.append(gt_fineTuned_coughdb);				

		gt_coughdb = []
		for i in range (0,len(fileNames)):
			gt_coughdb = gt_coughdb + gt_cdb[i]

		return gt_coughdb	

	def _gt_fineTuned_coughdb_filename(self, fileName):
		"""
		Temporary function for generating cough location do with filename
		TODO: Remove this function in future
		Return the database of groundd-truth cough data in the 
		List containing 3 things. 
		1 - Start point of cough
		2 - End point of cough
		3 - Input filename
		"""
		path = os.path.join(self._dataset_path, fileName + '_fineTuned.csv')
		with open(path, 'rb') as csvfile:
			lines = csv.reader(csvfile, delimiter=',')
			gt_fineTuned_coughdb = list(lines)	
			gt_fineTuned_coughdb = [[int(x) for x in y] for y in gt_fineTuned_coughdb]	
			
		print 'Number of GT coughs in file {} is {}'.format(fileName, len(gt_fineTuned_coughdb))
		return gt_fineTuned_coughdb	

	def _gen_location(self):
		"""	
		Return locations of cough and bg.
		This creates a crop centered at the groundTruth cough center. 
		"""
		cough_GTs = self._gt_fineTuned_coughdb()
		slidingWindow_size = cfg.FRAME_SIZE * self._fs

		gt_coughdb = []	
		for cough_GT in cough_GTs:
			centerPOS = int(math.floor((cough_GT[0]+cough_GT[1])/2))
			window_s = centerPOS - int(math.floor( slidingWindow_size / 2.0))+1
			window_e = centerPOS + int(math.floor( slidingWindow_size / 2.0))
			gt_db = []
			gt_db.append(window_s)
			gt_db.append(window_e)
			gt_db.append(1)
			gt_db.append(cough_GT[2])
			gt_db.append(cough_GT[0]-window_s)
			gt_db.append(cough_GT[1]-window_e)
			gt_coughdb.append(gt_db)

		return gt_coughdb

	def gt_coughdb(self):
		"""
		Returns database containing input features (MFCCs, Filterbank energies, etc) for the training cough locations depending on the cfg file 
		It also returns the ground Truth locations of the cough data with respect to the frame (Delta_start and Delta_end). 
		Writes the database into a file the first time to speed up the process afterwards. 
		"""
		fileNames = cfg.TRAIN.INPUT_FILES
		if self._split == 'test':
			fileNames = cfg.TEST.INPUT_FILES

		# Check if the file is pre-computed and present in cache directory. If present load it from the pkl file.
		cache_file = os.path.join(self.cache_path, self._name + '_' + self._split + '_' + fileNames +'_db.pkl')
		if os.path.exists(cache_file):
			with open(cache_file, 'rb') as fid:
				coughdb = cPickle.load(fid)
			print '{} data cough db is loaded from {}'.format(self._name, cache_file)
			print 'Total number of Training/Testing Data : {}'.format(len(coughdb['cough']['features'])*2)
			return coughdb
	
		coughdb = self._get_MFCC()
		print 'Total number of Training/Testing Data : {}'.format(len(coughdb['cough']['features'])*2)

		# Write the generated coughdb to cache file so as to make it faster next time. 
		with open(cache_file, 'wb') as fid:
			cPickle.dump(coughdb, fid, cPickle.HIGHEST_PROTOCOL)
         	print 'Caching coughdb to {}'.format(cache_file)

		return coughdb

	def create_test_files(self):
		"""
		This is a dummy function to create test audio files cropped for +/1 ve examples for cough App developement.
		creates small clips of .wav file
		"""
		fileNames = cfg.TRAIN.INPUT_FILES.split()
		if self._split == 'test':
			fileNames = cfg.TEST.INPUT_FILES.split()
		
		file_path = os.path.join(self._dataset_path, fileNames[0] + '.wav')
		(fs, x) = wav.read(file_path)
		bgFilePrefix = 'bg_'
		coughFilePrefix = 'cough_'
		gt_locs = self._gen_location()
		locArray = np.zeros(len(x))
		for gt in gt_locs:
			locArray[gt[0]:gt[1]] = 1

		# create 20 bg audio files
		windowSize = int(cfg.FRAME_SIZE * self._fs)
		for j in range(20):
			write_path = os.path.join(self._dataset_path, 'clips', bgFilePrefix + str(j) + '.wav')	
			start = np.random.randint(0, len(x)-windowSize - 1)
			end = start + windowSize - 1
			while sum(locArray[start:end]) != 0:
				start = np.random.randint(0, len(x)-windowSize)
				end = start + windowSize - 1
			s = x[start:end]
			wav.write(write_path, fs, s)	

		# create 20 cough audio files. 
		for i in range(20):
			write_path = os.path.join(self._dataset_path, 'clips', coughFilePrefix + str(i) + '.wav')
			s = x[gt_locs[i][0]:gt_locs[i][1]]
			wav.write(write_path, fs, s)

	def _get_MFCC(self):
		"""
		Returns database containing equal number of +ve and -ve cough examples according to the data generation mode.
		This is the function where MFCC and other features are computed for every training data. 
		Generates MFCCs + frequency band energies + delta of MFCCs
		labels[i] = '0' if for bg and '1' for cough
		"""
		gt_db = {}		# Dictionary to hold the ground truths
		coughdb = {}	# Dictionary to hold various parts of cough data for training
		bgdb = {}		# Dictornary to hold background points	

		features_cough = np.zeros((0,200), dtype=np.float32)	# Hardcoded feature dimension to 200 (50 x 4 | 50 for each sub-window)
		locations_cough = np.zeros((0,2), dtype=np.float32)		# location of gt_cough wrt frame
		labels_cough = []
		features_bg = np.zeros((0,200), dtype=np.float32)		# Hardcoded feature dimension to 200 (50 x 4 | 50 for each sub-window)
		locations_bg = np.zeros((0,2), dtype=np.float32)		# location of gt_cough wrt frame
		labels_bg = []

		gt_locs = self._gen_location()

		cur_loaded = gt_locs[0][3]
		file_path = os.path.join(self._dataset_path, gt_locs[0][3] + '.wav')
		(fs, x) = wav.read(file_path)

		# Cough ~~~~ 
		for gt in gt_locs:
			if gt[3] != cur_loaded:
				file_path = os.path.join(self._dataset_path, gt[3] + '.wav')
				(fs, x) = wav.read(file_path)
				cur_loaded = gt[3]
			
			s = x[gt[0]:gt[1]]
			mfcc_f = np.array(mfcc(s, fs, cfg.SUB_WINDOW_SIZE, cfg.SUB_WINDOW_STRIDE, 12, int(math.floor(3*math.log(fs))), 512, 0, None, 0, 0, True))
			fbank_f = np.array(logfbank(s,fs, cfg.SUB_WINDOW_SIZE, cfg.SUB_WINDOW_STRIDE, int(math.floor(3*math.log(fs))), 512, 0, None, 0))
			d_mfcc_f = np.array(delta(mfcc_f,2))		# 2 in the number of frames before and after each frame used to generate delta
			feat = np.reshape(np.hstack((mfcc_f, fbank_f, d_mfcc_f)), (1,-1))
			features_cough = np.vstack((features_cough, feat))
			loc = np.array([gt[4], gt[5]]) 	
			locations_cough = np.vstack((locations_cough, loc))
			labels_cough.append(1)

		coughdb['features']		= features_cough
		coughdb['locations']	= locations_cough
		coughdb['labels']		= labels_cough
		gt_db['cough']	= coughdb

		# Background ~~~~
		fileNames = cfg.TRAIN.INPUT_FILES.split()
		if self._split == 'test':
			fileNames = cfg.TEST.INPUT_FILES.split()
		windowSize = int(cfg.FRAME_SIZE * self._fs)

		for names in fileNames:
			gt_locs = self._gt_fineTuned_coughdb_filename(names)
			file_path = os.path.join(self._dataset_path, names + '.wav')
			(fs, x) = wav.read(file_path)	
			locArray = np.zeros(len(x))

			for gt in gt_locs: 
				locArray[gt[0]:gt[1]] = 1

			for i in range(len(gt_locs)):
				start = np.random.randint(0, len(x)-windowSize - 1)
				end = start + windowSize - 1
				while sum(locArray[start:end]) != 0:
					start = np.random.randint(0, len(x)-windowSize)
					end = start + windowSize - 1
				s = x[start:end]
				mfcc_f = np.array(mfcc(s, fs, cfg.SUB_WINDOW_SIZE, cfg.SUB_WINDOW_STRIDE, 12, int(math.floor(3*math.log(fs))), 512, 0, None, 0, 0, True))
				fbank_f = np.array(logfbank(s,fs, cfg.SUB_WINDOW_SIZE, cfg.SUB_WINDOW_STRIDE, int(math.floor(3*math.log(fs))), 512, 0, None, 0))
				d_mfcc_f = np.array(delta(mfcc_f,2))		# 2 in the number of frames before and after each frame used to generate delta
				feat = np.reshape(np.hstack((mfcc_f, fbank_f, d_mfcc_f)), (1,-1))
				features_bg = np.vstack((features_bg, feat))
				loc = np.array([gt[0], gt[1]])	# Dummy values
				locations_bg = np.vstack((locations_bg, loc))
				labels_bg.append(0)				

		bgdb['features']	= features_bg
		bgdb['locations']	= locations_bg
		bgdb['labels']		= labels_bg
		gt_db['bg']	= bgdb

		print 'Total number of cough {}, total number of background {}'.format(len(gt_db['cough']['features']), len(gt_db['bg']['features']))
		return gt_db
