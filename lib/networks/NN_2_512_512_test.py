# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

import tensorflow as tf
from networks.network import Network

n_classes = 2

class NN_2_512_512_test(Network):
	def __init__(self, trainable=True):
		self.name = 'NN_2_512_512'
		self.inputs = []
		self.MFCC_Feat = tf.placeholder(tf.float32, shape=[None, 200])
		self.gt_labels = tf.placeholder(tf.float32, shape=[None, 2])
		self.keep_prob = tf.placeholder(tf.float32)
		self.layers = dict({'MFCC_Feat':self.MFCC_Feat, 'gt_labels': self.gt_labels})
		self.trainable = trainable
		self.setup()

	def setup(self):
		(self.feed('MFCC_Feat')		# Feed forward network 
			 .fc(512, name='fc1')
			 .fc(512, name='fc2')
			 .fc(n_classes, relu=False, name='cls_score')
			 .softmax(name='cls_prob'))
