# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

""" Factory method to get the approriate network. """


# TODO: make this file correct hardcoded for manchester at the moment
__sets = {}

# List all the networks 
from networks.NN_2_512_512_test import NN_2_512_512_test
from networks.NN_2_512_512_train import NN_2_512_512_train
import pdb
import tensorflow as tf

# TODO: proper implementation of factory class
# for split in ['train', 'test']:
# 	name = 'NN_2_512_512_{}'.format(split)
# 	__sets[name] = (lambda split=split : NN_2_512_512(split))

# Hacky way to get things working initially. CHange it TODO
def get_network(name):
	"""Get a network by name"""
	if name == 'NN_2_512_512_test':
		return NN_2_512_512_test()
	elif name == 'NN_2_512_512_train':
		return NN_2_512_512_train()
	else: 
		raise KeyError('Unknown network: {}'.format(name))

def list_networks():
	""" List all registered networks. """
	return __sets.keys()	
