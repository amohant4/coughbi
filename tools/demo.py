# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

import _init_paths
from python_speech_features import mfcc
from python_speech_features import delta
from python_speech_features import logfbank
from networks.factory import get_network
from cough_NN.config import cfg
import scipy.io.wavfile as wav
import tensorflow as tf
import numpy as np
import math
import os
import sys

def test_demo(sess, net):

	blobs = {}
	features = np.zeros((0,200), dtype=np.float32)	
	labels = []

	testClipsPath = os.path.abspath(os.path.join(os.path.dirname(__file__),'..','Data','manchester','clips'))
	allFiles = os.listdir(testClipsPath)	
	for files in allFiles:
		path = os.path.join(testClipsPath, files)
		(fs, s) = wav.read(path)	
		mfcc_f = np.array(mfcc(s, fs, cfg.SUB_WINDOW_SIZE, cfg.SUB_WINDOW_STRIDE, 12, int(math.floor(3*math.log(fs))), 512, 0, None, 0, 0, True))
		fbank_f = np.array(logfbank(s,fs, cfg.SUB_WINDOW_SIZE, cfg.SUB_WINDOW_STRIDE, int(math.floor(3*math.log(fs))), 512, 0, None, 0))
		d_mfcc_f = np.array(delta(mfcc_f,2))
		feat = np.reshape(np.hstack((mfcc_f, fbank_f, d_mfcc_f)), (1,-1))
		features = np.vstack((features, feat))
		if files.split('_')[0] == 'cough':
			labels.append([0.,1.])
		else:
			labels.append([1.,0.])
	
	blobs['features'] = features
	blobs['labels'] = labels

	correct_prediction = tf.equal(tf.argmax(net.get_output('cls_prob'), 1), tf.argmax(net.get_output('gt_labels'), 1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))	
	
	feed_dict={net.MFCC_Feat: blobs['features'], net.keep_prob: 1.0, net.gt_labels: blobs['labels']}
	run_options = None
	run_metadata = None

	print 'Testing network ... '
	net_accuracy = sess.run([accuracy], feed_dict=feed_dict, options=run_options, run_metadata=run_metadata)
	print 'Demo Network accuracy is {}'.format(net_accuracy)

if __name__ == '__main__':
	network = get_network('NN_2_512_512_test')
	weights = os.path.abspath(os.path.join(os.path.dirname(__file__),'..','output', \
				'default','manchester','manchester_NN_2_512_512_iter_10000.ckpt'))
	saver = tf.train.Saver()
	sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
	saver.restore(sess, weights)	
	test_demo(sess, network)
