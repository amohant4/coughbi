# File: Demo.py
# Author: Abinash Mohanty
# Date: 02/28/2017
# Arizona State University
# Runs sample loading of audio data and generation of MFCC
# Will also create the framework structure for database creation

import _init_paths
from cough_NN.train import train_net
from datasets.factory import get_dataset
from cough_NN.config import cfg, get_output_dir
from networks.factory import get_network
import argparse
import os
import random
import sys

def parse_args():
	""" 
	Parse Input arguments
	"""
	# Dummy function at the moment. Will be useful in future when we create the framework TODO
	parser = argparse.ArgumentParser(description='Cough Detection Demo')
	parser.add_argument('--device', dest='device', help='device to use',
						default='cpu', type=str)
	parser.add_argument('--device_id', dest='device_id', help='device id to use',
						default=0, type=int)
	parser.add_argument('--iters', dest='max_iters',
						help='number of iterations to train',
						default=10000, type=int)
	parser.add_argument('--cfg', dest='cfg_file',
						help='optional config file',
						default=None, type=str)
	parser.add_argument('--dataset', dest='dataset',
						help='dataset to train on',
						default='manchester_trainval', type=str)
	parser.add_argument('--network', dest='network',
						help='name of the network',
						default='NN_2_512_512_train', type=str)
	
	if len(sys.argv) == 1:
		parser.print_help()
		sys.exit(1)
	
	args = parser.parse_args()
	return args

if __name__ == '__main__':

	args = parse_args()
	random.seed(cfg.RNG_SEED)

	dataset = get_dataset(args.dataset)
	max_iters=args.max_iters

	device_name = '/{}:{:d}'.format(args.device,args.device_id)
	print device_name

	output_dir = get_output_dir(dataset, None)
	print 'Output will be saved to {}'.format(output_dir)
	
	network = get_network(args.network)
	print 'Use network `{:s}` in training'.format(args.network)

	train_net(network, dataset, output_dir, max_iters)
