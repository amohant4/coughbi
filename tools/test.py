# --------------------------------------------------------
# Cough Detection NN
# Arizona State Univerity
# Written by Abinash Mohanty
# --------------------------------------------------------

import _init_paths
from cough_NN.test import test_net
from datasets.factory import get_dataset
from cough_NN.config import cfg, get_output_dir
from networks.factory import get_network
import tensorflow as tf
import argparse
import sys

def parse_args():
	""" 
	Parse Input arguments
	"""
	# Dummy function at the moment. Will be useful in future when we create the framework TODO
	parser = argparse.ArgumentParser(description='Cough Detection Demo')
	parser.add_argument('--device', dest='device', help='device to use',
						default='cpu', type=str)
	parser.add_argument('--device_id', dest='device_id', help='device id to use',
						default=0, type=int)
	parser.add_argument('--weights', dest='model',
						help='model to test',
						default=None, type=str)	
	parser.add_argument('--cfg', dest='cfg_file',
						help='optional config file',
						default=None, type=str)
	parser.add_argument('--dataset', dest='dataset',
						help='dataset to train on',
						default='manchester_test', type=str)
	parser.add_argument('--network', dest='network',
						help='name of the network',
						default='NN_2_512_512_test', type=str)
	
	if len(sys.argv) == 1:
		parser.print_help()
		sys.exit(1)
	
	args = parser.parse_args()
	return args

if __name__ == '__main__':
	args = parse_args()
	
	dataset = get_dataset(args.dataset)

	device_name = '/{}:{:d}'.format(args.device,args.device_id)
	print device_name

	network = get_network(args.network)
	print 'Use network {} in testing.'.format(args.network)

	saver = tf.train.Saver()
	sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
	saver.restore(sess, args.model)
	print ('Loading model weights from {:s}').format(args.model)

	test_net(sess, network, dataset)
